# misiones

react app para el festival de cine de misiones.
```
npm start para correrla
```

Esta pensado como un recorrido virtual

## DATA
* [link a drive con archivos de refe y assets de marca](https://drive.google.com/drive/folders/1kFUOzp9Gd-Wk6_sazxj53WXsk0ANpFNB)
* [link a cortos](https://docs.google.com/document/d/10MO-gieGTK3cDc9b1mqYee3sRpZFeUeLeOonVtmJZ2Y/edit?usp=sharing)

## TODO

* Cambiar los botones. En vez de background img poner un <img/> así podemos tintear las flechas

### Banners

* Hay por lo menos un banner en la refe que es menos alto. Se puede escalar la imagen pero queda mal el logo...
* Si queremos que los banners tengan sombra necesitamos recortarle un tuks los bordes de las imágenes

### Hosting y dominio
