import React, { useState, useEffect } from 'react'

//theme
import { ThemeProvider } from 'styled-components'
import { GlobalStyles } from './global'
import { theme } from './theme'
import styled from 'styled-components'


//views
import Banner from './components/Banner.js'

import Cortometrajes from './views/Cortometrajes.js'
import Innovacion from './views/Innovacion.js'
import Prensa from './views/Prensa.js'
import Videoteca from './views/Videoteca.js'
import Vivo from './views/Vivo.js'

// assets
//import bkg_2 from "./content/assets/img/background_2.png"
import foreground from "./content/assets/img/foreground.png"
import flecha from "./content/assets/img/flecha.png"
import flecha_negra from "./content/assets/img/flecha_negra.png"
import flecha_amarilla from "./content/assets/img/flecha_amarillo.png"
import flecha_verde from "./content/assets/img/flecha_verde.png"

import { MdScreenRotation } from 'react-icons/md'


const sections = {
  'prensa': {
    view: Prensa,
    title: 'Prensa',
    styles: {
      button: {
        top: "15%",
        left: "20%"
      },
      title: {
        top: "40%",
        left: "10%",
        fontSize: "2.8em"
      },
      banner: {
        left: "51%",
        top: "20%",
        transform: "perspective(500px) rotate3d(0, -1, 0, 18deg)",
        zIndex: 2
      },
      spot: {
        top: "-40%",
        filter: "brightness(1.5)"
      }
    },
    flecha: flecha
  },
  'innovación': {
    title: 'Innovación',
    view: Innovacion,
    styles: {
      button: {
        top: "35%",
        left: "20%",
        transform: "rotateZ(-45deg)"
      },
      title: {
        top: "20%",
        left: "10%",
        fontSize: "1.8em",
        color: theme.verde
      },
      banner: {
        left: "30%",
        top: "30%",
        transform: "perspective(500px) rotate3d(0, 1, 0, 15deg)"
      },
      spot: {
        top: "-60%",
        filter: "brightness(1.5)"
      }
    },
    flecha: flecha_verde
  },
  'videoteca': {
    title: 'Videoteca',
    view: Videoteca,
    styles: {
      button: {
        top: "20%",
        left: "20%",
      },
      title: {
        top: "50%",
        left: "10%",
        fontSize: "2em",
        color: theme.amarillo
      },
      banner: {
        left: "10%",
        top: "0%",
        transform: "perspective(800px) rotate3d(0, 1, 0, 15deg)"
      },
      spotBottom: {
        bottom: "-20%"
      }
    },
    flecha: flecha_amarilla
  },
  'cortometrajes': {
    title: 'Corto-metrajes',
    view: Cortometrajes,
    styles: {
      button: {
        top: "30%",
        left: "67%",
        transform: "rotateZ(45deg)",
        width: "25%",
        height: "10%"
      },
      title: {
        top: "30%",
        left: "10%",
        fontSize: "2.2em",
        hyphens: "manual",
        wordBreak: "break-word",
        color: theme.verde
      },
      banner: {
        left: "65%",
        top: "0%",
        transform: "perspective(800px) rotate3d(0, -1, 0, 15deg)"
      },
      spotBottom: {
        zIndex: 1
      }
    },
    flecha: flecha_verde
  },
  'escenario': {
    title: 'Escenario en vivo',
    view: Vivo,
    styles: {
      button: {
        top: "20%",
        left: "20%",
        transform: "rotateZ(90deg)",
      },
      title: {
        top: "45%",
        left: "0%",
        color: "black",
        fontSize: "2.1em",
        hyphens: "manual",
        wordBreak: "break-word",
        textAlign: "center",
        wordSpacing: "20px"
      },
      banner: {
        left: "80.5%",
        top: "30%",
        transform: "perspective(1000px) rotate3d(0, -1, 0, 25deg)",
        zIndex: 1
      },
      spot: {
        top: "-60%"
      }
    },
    flecha: flecha_negra
  }
}

const Container = styled.div`
  transform-origin: ${(props) => `${props.center.x}px ${props.center.y}px`}; // sin este punto y coma me rompia todo el syntax highlighting abajo jeje
`

function App() {

  const [atEntrance, setAtEntrance] = useState(true)
  const [atSection, setAtSection] = useState(null)
  const [center, setCenter] = useState({ x: 0, y: 0 })


  const [anim, setAnim] = useState(false)
  const animDuration = 1200

  useEffect(() => {
    document.title = "Movil Fest 2020"
    // para que se acomode el browser
    window.onorientationchange = function() {
      window.location.reload();
    }
  })


  const goToSection = (s, c) => {
    console.log('section clicked', s)
    console.log('center', c)

    setAtSection(s)
    setCenter(c)

    setTimeout(_ => {
      setAnim(true)
    }, 50)

    setTimeout(_ => {
      setAnim(false)
      setAtEntrance(false)
      setCenter({ x: 0, y: 0 })
    }, animDuration)
  }

  const goHome = () => {
    console.log('go home')

    setCenter({ x: "0px", y: "0px" })

    setTimeout(_ => {
      setAnim(true)
    }, 200)

    setTimeout(_ => {
      setAnim(false)
      setAtEntrance(true)
    }, animDuration)
  }

  const buildBanner = (section, idx) => {

    return <Banner
      key={idx}
      name={section}
      goToSection={goToSection}
      title={sections[section].title}
      style={sections[section].styles}
      flecha={sections[section].flecha}
    ></Banner>
  }


  const drawSection = () => {
    return sections[atSection].view({ goHome })
  }


  return (
    <ThemeProvider theme={theme}>
      <GlobalStyles />
      <Container
        id="app"
        center={{ x: center.x, y: center.y }}
        className={anim ? "bannerTransition" : ""}
      >
        <div id="orientationCard">
          <div id="orientationWrapper">
            <MdScreenRotation style={{ fontSize: '20em' }} />
            <h2>Por favor incline su dispositivo en modo apaisado para visualizar la experiencia.</h2>
          </div>
        </div>
        {atEntrance
          ? <div className="hall">
            <img
              src={foreground}
              className={anim ? "foreground blur" : "foreground"}
              alt="Imagen de hojas"
            />
            {Object.keys(sections).map(buildBanner)}
          </div>
          : drawSection()}
      </Container>
    </ThemeProvider>
  )
}

export default App;
