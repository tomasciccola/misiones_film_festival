import banner from "../content/assets/img/banner.png"
import spotBottom from "../content/assets/img/spot_reflejo.png"
import spot from "../content/assets/img/spot.png"

//import banner from "../content/assets/img/banner.png"

function Banner(props){
  const center = { x: 0, y: 0}
  return (
    <div 
      style={props.style.banner}
      className='banner'
      ref={el => {
        if(el){
          var rect = el.getBoundingClientRect()
          center.x = (rect.left) + 100 //SI, calculo el width a mano...
          center.y = rect.top + (rect.height/2) 
        }
      }}
    >
      <img 
        alt="imagen de una sombra de un spot luz" 
        style={props.style.spotBottom} 
        className="spotBottom" 
        src={spotBottom}
      />
      <img
        alt="imagen de un spot de luz" 
        style={props.style.spot} 
        className="spot" 
        src={spot}
      />
      <img 
        className="bannerBackground" 
        src={banner}
        alt="Imagen de un banner"
      />
      <h2 onClick={() =>{
          props.goToSection(props.name, center)
      }} 
      className="bannerTitle" 
      style={props.style.title}> 
      {props.title}
       </h2>
      <button className="bannerButton"
        onClick={() => {
          props.goToSection(props.name, center)
        }}
        style={props.style.button}
        >
    <img
      src={props.flecha} 
      alt="Imagen de un botón con una flecha"
    />
    </button>
    </div>
  )
}

export default Banner
