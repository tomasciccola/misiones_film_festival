import React, { useState } from 'react'
import ReactPlayer from 'react-player/youtube'
import styled from 'styled-components'

import { GiLaurelCrown } from 'react-icons/gi'

export default function CortoSlide(props) {
  const [visible, setVisible] = useState(true)

  const hideCard = () => {
    setVisible(false)
  }

  return (
    <div id="corto">
      <ReactPlayer url={props.url} width="100%" height="45vh" style={{}} onReady={hideCard} light controls />
      <Card visibility={visible} >
        <Titulo>{props.titulo}</Titulo>
        <h4>{props.autores}</h4>
        <h4>{props.year}</h4>
        <h4>{props.colegio ? props.colegio + ', ' : ''}{props.origen}</h4>
        <h4>{props.sinopsis}</h4>
        <Premio >
          <GiLaurelCrown />
          <h4>{props.premio}</h4>
        </Premio>
      </Card>
    </div>
  )
}


const Card = styled.section`
  display: ${props => props.visibility ? 'block' : 'none'};
  top: 0%;
  left: 0%;
  height: 100%;
  width: 37.3vw;
  position: absolute;
  /*
  width: 76.6%;
  height: 100%;
  position: absolute;
  top: 0%;
  left: 11.7%;
  */
  pointer-events: none;
  background-color: rgba(0,0,0,0.7);
  padding: 2em;
  backdrop-filter: blur(10px);
`

const Titulo = styled.h3`
  font-size: 2em;
`

const Premio = styled.div`
  display: flex;
  align-items: center;
  font-size: 2em;
  h4 {
    font-size: 0.7em;
    margin: 0px 0px 0px 10px;
  }
`