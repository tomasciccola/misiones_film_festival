
import buttonBkg from "../content/assets/img/flecha.png"

function GoBack(props) {
  return (
    <div className="goback" style={{ left: props.left, bottom: props.bottom }}>
      <button onClick={props.onClick} >
        <img
          src={props.src ? props.src : buttonBkg}
          alt="Imagen de un botón con una flecha"
        />
      </button>
    </div>
  )
}

export default GoBack
