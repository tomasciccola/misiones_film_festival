import styled from "styled-components"

const Logo = styled.img`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -265%);
  display: block;

  @media screen and (min-width: 320px) and (max-width: 767px) {
    transform: translate(-50%, -150%) scale(0.5);
  }
`

export default Logo