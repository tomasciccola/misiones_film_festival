import React from 'react';
import { StyledMenu } from './Menu.styled';
import { bool } from 'prop-types';

const Menu = ({ open, sectionChange }) => {
  function onSectionChange(event) {
    sectionChange(event)
  }

  return (
    <StyledMenu open={open}>
      <button onClick={() => onSectionChange('entrada')}>
        {/* <span role="img" aria-label="about us">&#x1f481;&#x1f3fb;&#x200d;&#x2642;&#xfe0f;</span> */}
        Entrada
      </button>
      <button onClick={() => onSectionChange('prensa')}>
        {/* <span role="img" aria-label="price">&#x1f4b8;</span> */}
        Prensa
        </button>
      <button onClick={() => onSectionChange('platoPrincipal')}>
        {/* <span role="img" aria-label="contact">&#x1f4e9;</span> */}
        Videoteca
        </button>
    </StyledMenu>
  )
}

// type checking
Menu.propTypes = {
    open: bool.isRequired,
}

export default Menu;
