import styled from "styled-components"
//import { DiPhotoshop, DiIllustrator, AiOutlineFontSize } from '@react-icons/all-files'
//
//import {DiPhotoshop} from 'react-icons'
//
import { DiPhotoshop, DiIllustrator } from 'react-icons/di'
import { FaFileImage, FaRegFileImage, FaYoutube }  from 'react-icons/fa'
import { MdMovie, MdLocalMovies } from 'react-icons/md'
import { AiOutlineFontSize } from 'react-icons/ai'


import { FaBeer } from 'react-icons/fa';

const iconsMap = {
  "DiPhotoshop": <DiPhotoshop/> ,
  "DiIllustrator":<DiIllustrator/>,
  "FaFileImage":<FaRegFileImage/> ,
  "FaRegFileImage":<FaRegFileImage/>,
  "FaYoutube":<FaYoutube/>, 
  "MdMovie":<MdMovie/>, 
  "MdLocalMovies":<MdLocalMovies/>, 
  "AiOutlineFontSize":<AiOutlineFontSize/>
}


function PrensaItem(props) {
  return (
    <ItemDiv>
      <ItemText>{props.text}</ItemText>
      <ItemIcon target="_blank" href={props.url} alt={'Descargar ' + props.text} >
        {iconsMap[props.icon]}
      </ItemIcon>
    </ItemDiv>
  )
}

const ItemDiv = styled.div`
  display: flex;
  align-items: center;
  justify-content:space-between;
`
const ItemText = styled.h3`
  margin-left:10px;
`

const ItemIcon = styled.a`
  padding-right: 20px;
  font-size:1.8em;
  text-decoration:none;
  color:white;
  transform:scale(1);

  :hover {
    transform: scale(1.1);
    transition:all 1s ease-in;
  }
`

export default PrensaItem
