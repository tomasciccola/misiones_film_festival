import { createGlobalStyle } from "styled-components"

import background from "./content/assets/img/background_1.png"
import background_2 from "./content/assets/img/background_2.png"
import flecha from "./content/assets/img/flecha.png"
import flechaNegra from "./content/assets/img/flecha_negra.png"

import bgVivo from "./content/assets/img/Escenario_Recital/BG2.png"
import bgInnovacion from "./content/assets/img/Innovacion/BG.jpg"
import bgPrensa from "./content/assets/img/Prensa/BackPrensa.png"

export const GlobalStyles = createGlobalStyle`
/*@import url('https://fonts.googleapis.com/css2?family=Montserrat&display=swap');*/
@import url('https://fonts.googleapis.com/css2?family=Montserrat:wght@800&display=swap');

html, body {
    margin: 0;
    padding: 0;
    overflow: hidden;
}

*, *::after, *::before {
    box-sizing: border-box;
}

html, body, #root, #app, #app {
  height: 100%;
  font-size: 1.7vmin;
}




#app {
  margin:0 auto;
  background: url(${background_2}) no-repeat center center,
              url(${background}) no-repeat center;
  font-family: 'Montserrat', sans-serif;
  color:white;
  background-color:black;
  overflow:hidden;
  animation: in 1.5s ease-in-out 1; 
}


#app > header {
  margin: 0 auto;
  text-align:center;
}

img.foreground {
  /*max-width:2437px; /* tamaño original de la imagen */
  width:250vmin;
  transform: translate(-5%,-15%);
  z-index:3;
  position:fixed;
  display:block;
  margin:0 auto;
  pointer-events:none;
  animation: in 1s ease-in 1;
}

#foreground.blur {
  backdrop-filter: blur(0px);
  filter: blur(0px);
  animation: foregroundBlur 4s ease-in-out;
}

@keyframes foregroundBlur {
  0%{
    backdrop-filter: blur(7px);
    filter: blur(5px);
  }

  100%{
    backdrop-filter: blur(0px);
    filter: blur(0px);
  }
}

.goback{
  position:absolute;
  width:5%;
  /*top:50%;*/
  bottom: 15%;
  left:50%;
  /* transform:translate(2vw,37vh); */
  z-index: 10;
  filter: drop-shadow(0 0 0.3em black);
}
@media screen and (min-width: 320px) and (max-width: 767px) {
  .goback {
    transform: scale(2);
    /* bottom: 10% !important; */
  }
  img.spot {
    display:none;
  }
  img.spotBottom {
    display:none;
  }
}
@media screen and (min-width: 1280px) {
  .goback {
    bottom: 4% !important;
  }
}

.goback button {
  background:none;
  /*
  position:absolute;
  width:60%;
  height:30%;
  z-index:1;
  */
  width:100%;
  border:none;
  outline:none;
  cursor: pointer;
  border-radius: 20px;
}

.goback button:hover {
  /*animation: glow;*/
   transition: all 0.3s ease-in;
}

@keyframes glow {
  0%{
  }

  100%{
    box-shadow: inset -3px -7px 9px 4px #2196F3;
  }
  
}

.goback button img{
  max-width:100%;
  max-height:100%;
  pointer-events:auto;
  transform: rotate(180deg);
  filter:invert(1);
}

.banner {
  height:60%;
  position:absolute;
  transition: all 1s ease-in-out;
  filter:blur(0px);
  animation: in 1s ease-in-out 1; 
}

@keyframes in{
  0% {
    filter:blur(10px);
  }

  100%{
    filter:blur(0px);
  }

}


.banner:hover {
  transform: scale(1.4)!important;
  z-index:4;
  transition: all 0.6s ease;
}

.bannerTransition {
  transform:scale(7)!important;
  filter:blur(10px);
  transition: transform 1s ease-in-out, filter 1s ease-in;
}

img.bannerBackground{
  max-width: 100%;
  max-height: 100%;
  position:relative;
  top:0;
}


.bannerButton {
  /*background: url(${flecha}) no-repeat center;
  background-size:contain;*/
  background:none;
  position:absolute;
  width:60%;
  height:30%;
  z-index:1;
  border:none;
  outline:none;
  top:10%;
  left:15%;
  cursor: pointer;
}


.bannerButton img {
  max-width:100%;
  max-height:100%;
  pointer-events:auto;
}

.bannerTitle {
  text-transform:uppercase;
  color: #233555;
  position:absolute;
  z-index:1;
  left:20%;
  top:50%;
  /*font-size:3em;*/
  font-size:4.5wv;
  cursor:pointer;
}


img.spot{
  position:absolute;
  top:-10%;
  left:-50%;
  mix-blend-mode: screen;
  pointer-events:none;
  transform:scale(1) translateY(0%);
  z-index:4;
  transition: transform  1s ease-in-out;
}

.banner:hover > img.spot {
  transform:scale(0.6) translateY(-40%);
  transition: all 1s ease-in-out;
}


 img.spotBottom {
  position:absolute;
  bottom:-15%;
  left:-88%;
  mix-blend-mode: screen;
  transform:scale(1);
  z-index:4;
  transition: transform z-index 1s ease-in-out;
}

.banner:hover > img.spotBottom {
  transform:scale(0.0);
  z-index:-1;
  transition: all 1s ease-in-out;
}

/* VIEWS */

.view {
  animation: in 2s ease;
}

#orientationCard {
  display: none;
}

/* ORIENTATION LOCK FOR MOBILE */
@media screen and (min-width: 320px) and (max-width: 767px) and (orientation: portrait) {
  /* html {
    transform: rotate(-90deg);
    transform-origin: left top;
    width: 100vh;
    overflow-x: hidden;
    position: absolute;
    top: 100%;
    left: 0;
  } */
  .hall{
    display: none;
  }
  #orientationCard {
    display: block;
    width: 95%;
    height: 95%;
    background-color: black;
    opacity: 0.9;
    backdrop-filter: blur(20px);
    /* transform: rotate(90deg); */
    text-align: center;
    margin: 5% auto;
  }
  #orientationWrapper {
    margin: auto;
    width: 90%;
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%,-50%);
  }
  h2 {
    font-size: 5em;
  }

}


.tituloSeccion {
  text-align: center;
}

.center {
  text-align: center;
}

/* PANTALLA VIVO */

#vivo {
  background: url(${bgVivo}) no-repeat center;
  position:relative;
  height:100vh;
  width:100vw;
  background-size:cover;
}

#logoVivo{
  width: 37vw;
  position:absolute;
  top: 50%;
  left: 50%;
  z-index:4;
  transform: translate(-50%, -52vh);
  /*
  left:25%;
  top:7%;
  transform:scale(0.8);
  */
}

#pantallaVivo{
 position: absolute;
 width:58vw;
 top: 50%;
 left: 50%;
 transform: translate(-50%, -50%);
}

#publico{
  position:absolute;
  bottom:-35px;
  left:50%;
  transform: translate(-50%, 0);
}

#liveStream {
 position: absolute;
 top: 50%;
 left: 50%;
 transform: translate(-50%, -53%);
}

#fgVivo {
  max-width: 2752px;
  transform: translate(-43%,-56%) scale(1.2);
  position: absolute;
  top: 50%;
  left: 50%;
}

/* CORTOS */

#fgCortos {
  max-width: 2842px;
  transform: translate(-50%,-50%) scale(1);
  position: absolute;
  top: 40%;
  left: 50%;
  z-index: 5;
  pointer-events: none;
  animation: fgCortosAnim 1.5s ease-in-out;
}
@keyframes fgCortosAnim {
  0% {
    transform: translate(-50%,-50%) scale(0.9);
  }
  100% {
    transform: translate(-50%,-50%) scale(1.0);
  }
}


/* INNOVACIÓN */

#innovacion {
  background: url(${bgInnovacion}) no-repeat center;
  position:relative;
  height:100vh;
  width:100vw;
  background-size:cover;
  /*
  height:100%;
  width:100%;
  */
}

#videosInnovacion {
  display:flex;
  flex-direction:column;
  position:absolute;
  justify-content: space-between;
  /*
  top:50%;
  left:50%;
  transform: translate(-120%,-55%);
  */
  top:5vh;
  /*left:5vw;*/
  left: 15%;
 /* z-index:9;*/
  width: auto;
  height: auto;
}

.vidWrapper {
  width: 40vw;
  height: 35vh;
  transform: translate(0, 7vh);
}

#fgInnovacion {
  width: 378vmin;
  max-width: 3752px;
  transform: translate(-50%,-50%);
  position: absolute;
  top: 50%;
  left: 60%;
  animation:fgInnovacionAnim 1.5s ease-in-out;
}

@keyframes fgInnovacionAnim {
  0% {
    width: 358vmin;
  }

  100%{
    width: 378vmin;
  }
}

/* PRENSA */

#prensaContainer{
  position:absolute;
  background: url(${bgPrensa}) no-repeat center center;
  width:100vw;
  height:89vh;
  top:50%;
  left:50%;
  transform:translate(-50%,-50%);
}

#prensaContent{
  max-width:70%;
  width:85vw;
  height:47vh;
  position:relative;
  top:50%;
  left:50%;
  display:flex;
  flex-direction:row;
  justify-content:space-evenly;
  flex-wrap:wrap;
  overflow-y:scroll;
  transform:translate(-49%,-50%);
}


#prensaContent::-webkit-scrollbar {
  width: 0.5em;
}
 
#prensaContent::-webkit-scrollbar-track {
  background: rgba(226, 226, 225, 0.4);
  border-radius:10px;
}
 
#prensaContent::-webkit-scrollbar-thumb {
  background-color: rgba(0.8,0.8,0.8);
  /* outline: 1px solid slategrey; */
  border-radius:10px;
  border: none;
}

.prensaCell {
  width:48%;
  background-color:black;
  opacity:0.8;
  backdrop-filter: blur(2px);
  /*border: 2px solid blue;*/
  margin:10px;
  padding:10px;
}


.fgPrensa {
  transform: translate(-50%,-10%) !important;
}




// CAROUSEL //////////////////////////////////////////////////////

.carousel {
  position: absolute;
  top: 56vh;
  left: 50.5%;
  transform: translate(-50%, -50%);
  width: 100%;
}

/* max-width constrains the width of our carousel to 550, but shrinks on small devices */
.carousel__container {
  max-width: 550px;
  margin: auto;
}

/* gives us the illusion of a "centered" slide */
.carousel__slider {
  padding-left: 25%;
  padding-right: 25%;
}
@media screen and (min-width: 320px) and (max-width: 767px) {
  .carousel__slider {
    height: 50vh;
    top: 5vh;
  }
  .sliderBotones {
    transform: scale(0.5);
    margin-top: -10vh;
  }
}

/* gives us the illusion of spaces between the slides */
.carousel__inner-slide {
  width: calc(100% - 20px);
  height: calc(100% - 20px);
  left: 10px;
  top: 10px;
}

#corto {
  top: 5%;
  height: 45vh;
  width: 37.3vw;
  left: 12%;
  position: relative;
}
// height en 45vh igual que el ReactPlayer de CortoSlide.js

/* CONTROLES DEL CAROUSEL */

.carousel__dot--selected {
  background-color: ${({ theme }) => theme.rojito} !important;
}
.carousel__dot {
  width: 16px;
  height: 16px;
  border-radius: 50%;
  border-style: none;
  color: white;
  margin-right: 10px;
}
.carousel__dot-group {
  margin-top: 30px;
}

.carousel__back-button, .carousel__next-button {
  margin: 30px;
  border-style: none;
  padding: 10px;
  background-image: url(${flechaNegra});
  filter: invert(1);
  background-size: 100%;
}
.carousel__back-button {
  transform: rotateZ(-90deg) scale(2);
}
.carousel__next-button {
  transform: rotateZ(90deg) scale(2);
}

`;
