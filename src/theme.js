export const theme = {
    background: '#323232',
    accent: '#461E66',
    accentDark: '#2E195A',
    /*verde: '#B7FD38',*/
    verde: '#14e802',
    rojito: '#DF274F',
    mobile: '576px',
    amarillo: '#e0ef4b'
}
