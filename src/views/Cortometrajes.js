import React from 'react';
import ReactPlayer from 'react-player/youtube'
import styled from 'styled-components'

import CortoSlide from '../components/CortoSlide'
import Logo from '../components/Logo'
import data from '../content/data/cortos.json'

import { CarouselProvider, Slider, Slide, ButtonBack, ButtonNext, DotGroup } from 'pure-react-carousel';
import 'pure-react-carousel/dist/react-carousel.es.css'; // webpack o rollup, probar sacar.
import { render } from '@testing-library/react';

import cortos_bg from "../content/assets/img/Escenario_Cortos/BG.jpg"
import logo from '../content/assets/img/Elemento_Logo.jpg'
import banderin from '../content/assets/img/Escenario_Cortos/Elemento_Banderin.png'
import ilum from '../content/assets/img/Escenario_Cortos/Elemento_Iluminacion.png'
import foreground from '../content/assets/img/Escenario_Cortos/Entorno.png'

import GoBack from '../components/GoBack'

export default function Cortometrajes(props) {

  return (
    <CortometrajesComponent className="view" >

      {/* <h2 className="tituloSeccion">Cortometrajes</h2> */}

      <Logo src={logo} alt="Movil Fest Logo" />

      <img src={foreground} id="fgCortos" alt="plantitas" />

      <h2>PROXIMAMENTE</h2>

      <CarouselProvider
        naturalSlideWidth={100}
        naturalSlideHeight={125}
        visibleSlides={1}
        currentSlide={0}
        totalSlides={data.cortos.length}
        infinite={true}
        isIntrinsicHeight={true}
      >
        <Slider>
          {data.cortos.map((corto, i) => {
            // console.log('data', i, corto)
            return (
              <Slide index={i} className="slider">
                <CortoSlide
                  titulo={corto.nombre}
                  autores={corto.autores}
                  url={corto.url}
                  premio={corto.premio}
                  sinopsis={corto.sinopsis}
                />
              </Slide>
            )
          })}
        </Slider>
        <div className={"center sliderBotones"}>
          <DotGroup />
          <ButtonBack></ButtonBack>
          <ButtonNext></ButtonNext>
        </div>
      </CarouselProvider>
      <GoBack onClick={props.goHome} left="48%" bottom="10%" />
    </CortometrajesComponent >
  )
}



const CortometrajesComponent = styled.div`
  background: url(${cortos_bg}) no-repeat center;
  position: relative;
  height: 100vh;
  width: 100vw;
  background-size: cover;

  /*
   *
   * CUANDO ESTÉN LISTOS LOS CORTOS PODEMOS MOSTRAR EL CAROUSEL 
   * Y BORRAR EL H2,
   * O NO,
   * TOTAL NADIE LO VE...
   * MUAJAJAJAJ
   *
   */

  h2 { 
    display:none;
    position:absolute;
    top:45%;
    left:50%;
    transform:translate(-50%,-50%);
    font-size:3em;
    color:black;
    text-align:center;
  }
`
