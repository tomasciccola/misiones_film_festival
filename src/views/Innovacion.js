import ReactPlayer from 'react-player/youtube'
import GoBack from '../components/GoBack'

import entorno from "../content/assets/img/Innovacion/Entorno.png"
import back from "../content/assets/img/flecha_negra.png"

const video_2 = "https://youtu.be/jcXZ9Qa3axo"
const video_1 = "https://youtu.be/QfDY50JgZmc"



function Innovacion(props) {
  return (
    <div id="innovacion" className="view">
      <img
        className="foreground"
        id="fgInnovacion"
        src={entorno}
      />
      <div id="videosInnovacion" >
        <div 
             className="vidWrapper"
             style={{ marginBottom: "10px"}}
        >
          <ReactPlayer
            url={video_1}
            controls
            width="100%"
            height="100%"
            className=""
          />
        </div>
        {/* cuando esté el video del holograma que falta lo mostramos */}
        <div className="vidWrapper">
          <ReactPlayer
            url={video_2}
            controls
            width="100%"
            height="100%"
          />
        </div>
      </div>
      <GoBack src={back} left="68%" onClick={props.goHome} ></GoBack>
    </div>
  )
}

export default Innovacion
