import React from 'react';
import styled from 'styled-components'

import Logo from '../components/Logo'

import logo from '../content/assets/img/Elemento_Logo.jpg'
import prensa_bg from '../content/assets/img/Prensa/BG.png'
import foreground from '../content/assets/img/Prensa/Entorno.png'

import data from '../content/data/prensa.json'

import GoBack from '../components/GoBack'

import PrensaItem from '../components/PrensaItem'

// import {  } from 'react-icons'


export default function Prensa(props) {
  const buildPrensaContent = (key, idx) => {

    const buildPrensaCell = (elem, idx) => {
      //icon, url, text
      return (
        <PrensaItem key={idx} text={elem["texto"]} url={elem["link"]} icon={elem["icon"]} ></PrensaItem>
      )
    }

    return(
      <div key={key} className="prensaCell">
        {data[key].map(buildPrensaCell)}
      </div>
    )
  }

  return (
    <PrensaComponent className="view">
      <img src={foreground} id="fgCortos" className="fgPrensa" alt="Plantitas" />
      <div id="prensaContainer">
        <div id="prensaContent">
        {Object.keys(data).map(buildPrensaContent)}
        </div>
      </div>
      <GoBack onClick={props.goHome} left="46%" />
    </PrensaComponent>
  )
}


const PrensaComponent = styled.div`
  background: url(${prensa_bg}) no-repeat center;
  position: relative;
  height: 100%;
`

const PrensaBack = styled(Logo)`
  transform: translate(-50%, -50%);
`
