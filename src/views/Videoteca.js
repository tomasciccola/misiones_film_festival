import React from 'react';
import styled from 'styled-components'

import CortoSlide from '../components/CortoSlide'
import Logo from '../components/Logo'
import data from '../content/data/videoteca.json'

import { CarouselProvider, Slider, Slide, ButtonBack, ButtonNext, DotGroup } from 'pure-react-carousel';
import 'pure-react-carousel/dist/react-carousel.es.css'; // webpack o rollup, probar sacar.
import { render } from '@testing-library/react';

import logo from '../content/assets/img/Elemento_Logo.jpg'
import videoteca_bg from '../content/assets/img/Escenario_Videoteca/BG2.jpg'
import peli from '../content/assets/img/Escenario_Videoteca/ElementoPelicula.png'
import dvd from '../content/assets/img/Escenario_Videoteca/Elemento_DVDs.png'
import foreground from '../content/assets/img/Escenario_Videoteca/Entorno.png'

import GoBack from '../components/GoBack'


export default function Videoteca(props) {

  return (
    <VideotecaComponent className="view">

      {/* <h2 className="tituloSeccion">Videoteca</h2> */}

      <Logo src={logo} alt="Movil Fest Logo" />

      <img src={foreground} id="fgCortos" alt="Plantitas" />
      {/* <Pelis src={peli} alt="Pelicula" />
      <Dvd src={dvd} alt="DVDs" /> */}

      <CarouselProvider
        naturalSlideWidth={100}
        naturalSlideHeight={125}
        visibleSlides={1}
        currentSlide={1}
        totalSlides={data.cortos.length}
        infinite={true}
        isIntrinsicHeight={true}
      >
        <Slider>
          {data.cortos.map((corto, i) => {
            // console.log('data', i, corto)
            return (
              <Slide index={i} className="slider">
                <CortoSlide
                  titulo={corto.nombre}
                  autores={corto.autores}
                  year={corto.year}
                  url={corto.url}
                  premio={corto.premio}
                  origen={corto.origen}
                  colegio={corto.colegio}
                />
              </Slide>
            )
          })}
        </Slider>
        <div className='center sliderBotones'>
          <DotGroup />
          <ButtonBack></ButtonBack>
          <ButtonNext></ButtonNext>
        </div>
      </CarouselProvider>
      <GoBack onClick={props.goHome} left="48vw" bottom="5%" />
    </VideotecaComponent >
  )
}


const VideotecaComponent = styled.div`
  background: url(${videoteca_bg}) no-repeat center;
  position: relative;
  height: 100%;
`

const Pelis = styled(Logo)`
  transform: translate(-50%, -360%);
`
const Dvd = styled(Logo)`
  transform: translate(-50%, -200%);
`