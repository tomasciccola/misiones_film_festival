import ReactPlayer from 'react-player/youtube'
import GoBack from "../components/GoBack"

import pantalla from "../content/assets/img/Escenario_Recital/Elemento_Pantalla.png"
import publico from "../content/assets/img/Escenario_Recital/Publico.png"
import entorno from "../content/assets/img/Escenario_Recital/ENtorno.png"
import logo from "../content/assets/img/Escenario_Recital/Logo.png"

const streamUrl = "https://youtu.be/L-HNC3tywwY"

function Vivo(props){
  return (
    <div id="vivo" className="view">
      <img 
        src={logo}
        id="logoVivo"
      />
      <img 
        className="foreground"
        id="fgVivo"
        src={entorno}
      />
      <img 
        id="pantallaVivo"
        src={pantalla}
      />
      <img 
        id="publico"
        src={publico}
      />
      <ReactPlayer 
        id="liveStream" 
        url={streamUrl} 
      controls 
      width="52vw" height="51vh"/>
      <GoBack left="45%" bottom="10%" onClick={props.goHome}></GoBack>
    </div>
  )
}

export default Vivo


